<?php

namespace Drupal\bs_slider;

/**
 * Defines an interface for BS Slider plugin options.
 */
interface BsSliderPluginOptionInterface {

  /**
   * Get plugin option value.
   *
   * @param string $name
   *  Option name.
   *
   * @return string
   *   Option value or NULL if option does not exist.
   */
  public function getPluginOptionValue($name);

  /**
   * Get plugin options for a given option name.
   *
   * Usually this is used to return possible options for a select widget.
   *
   * @param string $name
   *   Option name.
   *
   * @return array
   *   Array of options or NULL if option does not exist.
   */
  public function getPluginOptions($name);

}
