<?php

namespace Drupal\bs_slider\Form;

use Drupal\bs_slider\Plugin\BsSliderManager;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class BsSliderForm.
 */
class BsSliderConfigurationForm extends EntityForm {

  /**
   * The BS Slider plugin manager.
   *
   * @var \Drupal\bs_slider\Plugin\BsSliderManager
   */
  protected $bsSliderManager;

  /**
   * GeneralSettingsForm constructor.
   *
   * @param \Drupal\bs_slider\Plugin\BsSliderManager
   *   The paragraphs type feature manager service.
   */
  public function __construct(BsSliderManager $bsSliderManager) {
    $this->bsSliderManager = $bsSliderManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.bs_slider')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    // Change page title for the duplicate operation.
    if ($this->operation === 'duplicate') {
      $this->entity = $this->entity->createDuplicate();
    }

    /** @var \Drupal\bs_slider\Entity\BsSliderConfigurationInterface $bs_slider_configuration */
    $bs_slider_configuration = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $this->operation === 'duplicate' ? $this->t('Duplicate of %label', ['%label' => $bs_slider_configuration->label()]) : $bs_slider_configuration->label(),
      '#description' => $this->t('Label for the BS Slider.'),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $bs_slider_configuration->id(),
      '#machine_name' => [
        'exists' => '\Drupal\bs_slider\Entity\BsSliderConfiguration::load',
      ],
      '#disabled' => !$bs_slider_configuration->isNew(),
    ];

    $definitions = $this->bsSliderManager->getDefinitions();
    $plugin_options = [];
    foreach ($definitions as $key => $definition) {
      $plugin_options[$key] = $definition['label'];
    }

    if (empty($plugin_options)) {
      $form['plugins_missing_msg'] = [
        '#markup' => $this->t('There are no <em>BS Slider</em> plugins installed on the system. Please install a plugin before configuring this slider.'),
      ];

      return $form;
    }

    if ($form_state->hasValue('plugin_id')) {
      $plugin_id = $form_state->getValue('plugin_id');
    }
    elseif (!$bs_slider_configuration->isNew() || $this->operation === 'duplicate') {
      $plugin_id = (!$bs_slider_configuration->isNew() || $this->operation === 'duplicate') ? $bs_slider_configuration->getPluginId() : array_key_first($plugin_options);
    }
    else {
      $plugin_id = array_key_first($plugin_options);
    }

    $id = 'bs-slider-plugin-options';
    $form['plugin_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Type'),
      '#default_value' => $plugin_id,
      '#options' => $plugin_options,
      '#ajax' => [
        'callback' => '::updateSettingsAjax',
        'wrapper' => $id,
      ],
    ];

    if ($plugin_id) {
      $form['options'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Plugin options'),
        '#tree' => TRUE,
        '#prefix' => '<div id="' . $id . '">',
        '#suffix' => '</div>',

      ];

      /** @var \Drupal\bs_slider\Plugin\BsSliderInterface $plugin */
      $plugin = $this->bsSliderManager->createInstance($plugin_id, $bs_slider_configuration->getOptions());
      $form = $plugin->buildConfigurationForm($form, $form_state);
    }

    return $form;
  }

  /**
   * Ajax callback that updates field widget display settings fieldset.
   */
  public function updateSettingsAjax(array $form, FormStateInterface $form_state) {
    return $form['options'];
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\bs_slider\Entity\BsSliderConfigurationInterface $bs_slider_configuration */
    $bs_slider_configuration = $this->entity;
    $status = $bs_slider_configuration->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label BS Slider.', [
          '%label' => $bs_slider_configuration->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label BS Slider.', [
          '%label' => $bs_slider_configuration->label(),
        ]));
    }

    $form_state->setRedirectUrl($bs_slider_configuration->toUrl('collection'));

    return $status;
  }

}
