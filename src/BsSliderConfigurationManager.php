<?php

namespace Drupal\bs_slider;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implements BsSliderManagerInterface.
 */
class BsSliderConfigurationManager implements BsSliderConfigurationManagerInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected RendererInterface $renderer;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected CacheBackendInterface $cache;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected AccountProxyInterface $currentUser;

  /**
   * Constructs a BlazyManager object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ModuleHandlerInterface $module_handler, RendererInterface $renderer, ConfigFactoryInterface $config_factory, CacheBackendInterface $cache, AccountProxyInterface $currentUser) {
    $this->entityTypeManager = $entity_type_manager;
    $this->moduleHandler     = $module_handler;
    $this->renderer          = $renderer;
    $this->configFactory     = $config_factory;
    $this->cache             = $cache;
    $this->currentUser = $currentUser;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('module_handler'),
      $container->get('renderer'),
      $container->get('config.factory'),
      $container->get('cache.default'),
      $container->get('current_user')
    );
  }

  /**
   * Returns the entity type manager.
   */
  public function getEntityTypeManager() {
    return $this->entityTypeManager;
  }

  /**
   * Returns the module handler.
   */
  public function getModuleHandler() {
    return $this->moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public function getRenderer() {
    return $this->renderer;
  }

  /**
   * Returns the config factory.
   */
  public function getConfigFactory() {
    return $this->configFactory;
  }

  /**
   * Returns the cache.
   */
  public function getCache() {
    return $this->cache;
  }

  /**
   * {@inheritdoc}
   */
  public function configLoad(string $setting_name = '', string $settings = 'bs_slider.options') {
    $config  = $this->configFactory->get($settings);
    $configs = $config->get();
    return empty($setting_name) ? $configs : $config->get($setting_name);
  }

  /**
   * {@inheritdoc}
   */
  public function entityLoad($id) {
    return $this->entityTypeManager->getStorage('bs_slider')->load($id);
  }

  /**
   * {@inheritdoc}
   */
  public function entityLoadMultiple($ids = NULL) {
    return $this->entityTypeManager->getStorage('bs_slider')->loadMultiple($ids);
  }

  /**
   * {@inheritdoc}
   */
  public function entityLoadByProperties(array $values = []) {
    return $this->entityTypeManager->getStorage('bs_slider')->loadByProperties($values);
  }

  /**
   * {@inheritdoc}
   */
  public function getAllOptionSet() {
    /** @var \Drupal\bs_slider\Entity\BsSliderConfiguration[] $entities */
    $entities = $this->entityLoadMultiple();
    $option_sets = [];

    foreach ($entities as $option_set) {
      $option_sets[$option_set->id()] = $option_set->label();
    }

    return $option_sets;
  }

  /**
   * {@inheritdoc}
   */
  public function getAllOptionSetByProperties(array $values = []) {
    /** @var \Drupal\bs_slider\Entity\BsSliderConfiguration[] $entities */
    $entities = $this->entityLoadByProperties($values);
    $option_sets = [];

    foreach ($entities as $options_set_id => $option_set) {
      $option_sets[$option_set->id()] = $option_set->label();
    }

    return $option_sets;
  }

  /**
   * {@inheritdoc}
   */
  public function getOptionSet($name) {
    /** @var \Drupal\bs_slider\Entity\BsSliderConfiguration $entity */
    $entity = $this->entityLoad($name);

    return $entity ? $entity->getOptions() : [];
  }

  /**
   * {@inheritdoc}
   */
  public function getOptionSetDescription(array $bs_slider_optionset_options = NULL) {
    if (is_null($bs_slider_optionset_options)) {
      $bs_slider_optionset_options = $this->getAllOptionSet();
    }

    if (empty($bs_slider_optionset_options)) {
      return t('There are no BS Slider configurations available.');
    }

    $description = '';
    $bs_slider_link = Url::fromRoute('entity.bs_slider.collection');
    if ($bs_slider_link->access($this->currentUser)) {
      $description =
        t('To have more options, go to the <a href=":link">BS Slider configuration page</a> and add new configurations there.', [
          ':link' => $bs_slider_link->toString(),
        ]);
    }

    return $description;
  }

  /**
   * {@inheritdoc}
   */
  public function getPlugin($id) {
    /** @var \Drupal\bs_slider\Entity\BsSliderConfigurationInterface $bs_slider */
    if ($bs_slider = $this->entityLoad($id)) {
      /** @var \Drupal\bs_slider\Plugin\BsSliderManager $plugin_manager */
      $plugin_manager = \Drupal::service('plugin.manager.bs_slider');
      return $plugin_manager->createInstance($bs_slider->getPluginId(), $bs_slider->getOptions());
    }

    return NULL;
  }

}
