<?php

namespace Drupal\bs_slider\Plugin;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides the BS Slider plugin manager.
 */
class BsSliderManager extends DefaultPluginManager {

  /**
   * Constructs a new BsSliderManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/BsSlider', $namespaces, $module_handler, 'Drupal\bs_slider\Plugin\BsSliderInterface', 'Drupal\bs_slider\Annotation\BsSlider');

    $this->alterInfo('bs_slider_bs_slider_info');
    $this->setCacheBackend($cache_backend, 'bs_slider_bs_slider_plugins');
  }

}
