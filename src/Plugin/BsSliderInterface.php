<?php

namespace Drupal\bs_slider\Plugin;

use Drupal\bs_slider\BsSliderPluginOptionInterface;
use Drupal\bs_slider\Entity\BsSliderConfigurationInterface;
use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Defines an interface for BS Slider plugins.
 */
interface BsSliderInterface extends PluginInspectionInterface, PluginFormInterface, ConfigurableInterface {

  /**
   * Returns elements for formatter form.
   *
   * @param array $form
   * @param FormStateInterface $form_state
   * @param BsSliderPluginOptionInterface $plugin_option
   *
   * @return array
   */
  public function buildPluginOptionsForm(array $form, FormStateInterface $form_state, BsSliderPluginOptionInterface $plugin_option);

  /**
   * Get plugin description.
   *
   * @return string
   */
  public function getDescription();

  /**
   * Adds a default set of helper variables for preprocessors and templates.
   *
   * This preprocesses function is the first in the sequence of preprocessing
   * functions that are called when preparing variables of a paragraph.
   *
   * @param array $variables
   *   An associative array containing:
   *   - elements: An array of elements to display in view mode.
   *   - paragraph: The paragraph object.
   *   - view_mode: The view mode.
   */
  public function preprocess(array &$variables);

  /**
   * Extends the slider render array with behavior.
   *
   * @param array &$build
   *   A renderable array representing the slider. The module may add
   *   elements to $build prior to rendering. The structure of $build is a
   *   renderable array as expected by drupal_render().
   * @param \Drupal\bs_slider\Entity\BsSliderConfigurationInterface $bs_slider
   *   The paragraph.
   * @param array $options
   *   Additional options needed for view, plugin dependent.
   */
  public function view(array &$build, BsSliderConfigurationInterface $bs_slider, array $options = []);

}
