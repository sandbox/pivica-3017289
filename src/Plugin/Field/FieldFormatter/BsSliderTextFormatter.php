<?php

namespace Drupal\bs_slider\Plugin\Field\FieldFormatter;

use Drupal\bs_slider\BsSliderConfigurationManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'BS Slider Text' formatter.
 *
 * @FieldFormatter(
 *   id = "bs_slider_text",
 *   label = @Translation("BS Slider Text"),
 *   field_types = {
 *     "text",
 *     "text_long",
 *     "text_with_summary",
 *   },
 *   quickedit = {"editor" = "disabled"}
 * )
 */
class BsSliderTextFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  use BsSliderFormatterTrait;

  /**
   * Constructs a BsSliderTextFormatter instance.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, BsSliderConfigurationManagerInterface $manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->manager   = $manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('bs_slider_configuration.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return ['bs_slider' => 'default'];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return $this->getSettingsFormElements($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    // Early opt-out if the field is empty.
    if ($items->isEmpty()) {
      return [];
    }

    $build = [];

    /** @var \Drupal\bs_slider\Entity\BsSliderConfigurationInterface $bs_slider */
    $bs_slider = $this->manager->entityLoad($this->getSetting('bs_slider'));

    /** @var \Drupal\bs_slider\Plugin\BsSliderBase $plugin */
    $plugin = $this->manager->getPlugin($this->getSetting('bs_slider'));
    $plugin->view($build, $bs_slider, ['view_mode' => $this->viewMode]);

    // The ProcessedText element already handles cache context & tag bubbling.
    // @see \Drupal\filter\Element\ProcessedText::preRenderText()
    foreach ($items as $key => $item) {
      $element = [
        '#type'     => 'processed_text',
        '#text'     => $item->value,
        '#format'   => $item->format,
        '#langcode' => $item->getLangcode(),
      ];
      $build['#items'][$key] = $element;
      unset($element);
    }

    return $build;
  }

}
