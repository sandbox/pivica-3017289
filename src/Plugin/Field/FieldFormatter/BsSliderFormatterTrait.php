<?php

namespace Drupal\bs_slider\Plugin\Field\FieldFormatter;

use Drupal\bs_slider\BsSliderPluginOptionInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;

/**
 * A Trait common for bs_slider formatters.
 */
trait BsSliderFormatterTrait {

  /**
   * The bs_slider field formatter manager.
   *
   * @var \Drupal\bs_slider\BsSliderConfigurationManagerInterface
   */
  protected $manager;

  /**
   * Returns the bs_slider service.
   *
   * @return \Drupal\bs_slider\BsSliderConfigurationManagerInterface
   */
  public function manager() {
    return $this->manager;
  }

  /**
   * Get base formatter settings form elements.
   *
   * @param array $form
   * @param FormStateInterface $form_state
   * @return array
   *   Elements form array.
   */
  public function getSettingsFormElements(array $form, FormStateInterface $form_state) {
    $id = Html::getUniqueId('bs-slider-formatter-options');
    $elements = [
      '#prefix' => '<div id="' . $id . '">',
      '#suffix' => '</div>',
    ];

    $options = ['' => t('- Select -')] + $this->manager->getAllOptionSet();

    $elements['bs_slider'] = [
      '#type' => 'select',
      '#title' => $this->t('BS Slider'),
      '#description' => t('BS Slider configuration used for this slider. ') . $this->manager->getOptionSetDescription($this->manager->getAllOptionSet()),
      '#required' => TRUE,
      '#options' => $options,
      '#default_value' => $this->getSetting('bs_slider'),
      '#ajax' => [
        'callback' => [$this, 'updateSettingsAjax'],
        'wrapper' => $id,
      ],
    ];

    // Get current active plugin, so we can call correct plugin when we are in
    // ajax.
    $field_name = $this->fieldDefinition->getName();
    $bs_slider_active_plugin = $form_state->getValue(['fields', $field_name, 'settings_edit_form', 'settings', 'bs_slider'], $this->getSetting('bs_slider'));

    // Attach plugin formatter form elements.
    /** @var \Drupal\bs_slider\Plugin\BsSliderInterface $plugin_manager */
    if ($bs_slider_active_plugin && $plugin = $this->manager->getPlugin($bs_slider_active_plugin)) {
      if ($this instanceof BsSliderPluginOptionInterface) {
        $plugin_elements = $plugin->buildPluginOptionsForm($form, $form_state, $this);
        if (!empty($plugin_elements)) {
          $elements['options'] = [
              '#type' => 'fieldset',
              '#title' => $this->t('Plugin options'),
              '#tree' => TRUE,
            ] + $plugin_elements;
        }
      }
    }

    return $elements;
  }

  /**
   * Ajax callback that updates field widget display settings fieldset.
   */
  public function updateSettingsAjax(array $form, FormStateInterface $form_state) {
    $field_name = $this->fieldDefinition->getName();
    return $form['fields'][$field_name]['plugin']['settings_edit_form']['settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    if ($bs_slider = $this->manager->entityLoad($this->getSetting('bs_slider'))) {
      $summary[] = $this->t('BS Slider: @name', ['@name' => $bs_slider->label()]);
    }

    return $summary;
  }

}
