<?php

namespace Drupal\bs_slider\Plugin\Field\FieldFormatter;

use Drupal\Core\Form\FormStateInterface;

/**
 * BS Slider media gallery formatter.
 *
 * @FieldFormatter(
 *   id = "bs_slider_media_gallery",
 *   label = @Translation("BS Slider Media Gallery"),
 *   description = @Translation("Display the media referenced entities as a BS Slider Gallery."),
 *   field_types = {
 *     "entity_reference"
 *   },
 *   quickedit = {
 *     "editor" = "disabled"
 *   }
 * )
 */
class BsSliderMediaGalleryFormatter extends BsSliderEntityReferenceFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $settings = parent::defaultSettings();

    // Gallery thumbnail and view item view mode.
    $settings['options']['thumbnail_view_mode'] = '';
    $settings['options']['item_view_mode'] = '';

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    if ($this->getSetting('bs_slider') && $bs_slider = $this->manager->entityLoad($this->getSetting('bs_slider'))) {
      $summary[] = $this->t('BS Slider: @name', ['@name' => $bs_slider->label()]);
    }

    $view_modes = $this->getPluginOptions('target_field_view_modes');

    $view_mode = $this->getPluginOptionValue('thumbnail_view_mode');
    $summary[] = t('Rendered thumbnail as @mode', ['@mode' => $view_modes[$view_mode] ?? $view_mode]);

    $item_view_mode = $this->getPluginOptionValue('item_view_mode');
    $summary[] = t('Rendered item as @mode', ['@mode' => $view_modes[$item_view_mode] ?? $item_view_mode]);

    return $summary;
  }

}
