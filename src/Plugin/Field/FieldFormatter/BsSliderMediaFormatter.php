<?php

namespace Drupal\bs_slider\Plugin\Field\FieldFormatter;

/**
 * Plugin implementation of the BS Slider media formatter.
 *
 * @FieldFormatter(
 *   id = "bs_slider_media",
 *   label = @Translation("BS Slider Media"),
 *   description = @Translation("Display the referenced entities as a BS slider."),
 *   field_types = {
 *     "entity_reference"
 *   },
 *   quickedit = {
 *     "editor" = "disabled"
 *   }
 * )
 */
class BsSliderMediaFormatter extends BsSliderEntityReferenceFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $settings = parent::defaultSettings();

    // Media item view mode.
    $settings['options']['view_mode'] = '';

    return $settings;
  }

}
