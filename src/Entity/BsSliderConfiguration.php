<?php

namespace Drupal\bs_slider\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the BS Slider entity.
 *
 * @ConfigEntityType(
 *   id = "bs_slider",
 *   label = @Translation("BS Slider"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\bs_slider\BsSliderConfigurationListBuilder",
 *     "form" = {
 *       "add" = "Drupal\bs_slider\Form\BsSliderConfigurationForm",
 *       "edit" = "Drupal\bs_slider\Form\BsSliderConfigurationForm",
 *       "duplicate" = "Drupal\bs_slider\Form\BsSliderConfigurationForm",
 *       "delete" = "Drupal\bs_slider\Form\BsSliderConfigurationDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\bs_slider\BsSliderHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "configuration",
 *   config_export = {
 *     "id",
 *     "label",
 *     "status",
 *     "plugin_id",
 *     "options",
 *   },
 *   admin_permission = "administer bs_slider",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "status" = "status",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/configuration/media/bs_slider/{bs_slider}",
 *     "add-form" = "/admin/configuration/media/bs_slider/add",
 *     "edit-form" = "/admin/configuration/media/bs_slider/{bs_slider}/edit",
 *     "duplicate-form" = "/admin/configuration/media/bs_slider/{bs_slider}/duplicate",
 *     "delete-form" = "/admin/configuration/media/bs_slider/{bs_slider}/delete",
 *     "collection" = "/admin/configuration/media/bs_slider"
 *   }
 * )
 */
class BsSliderConfiguration extends ConfigEntityBase implements BsSliderConfigurationInterface {

  /**
   * The BS Slider ID.
   *
   * @var string
   */
  protected ?string $id;

  /**
   * The BS Slider label.
   *
   * @var string
   */
  protected string $label;

  /**
   * The BS Slider plugin ID.
   *
   * @var string
   */
  protected string $plugin_id;

  /**
   * The BS Slider plugin options.
   *
   * @var array
   */
  protected array $options = [];

  /**
   * {@inheritdoc}
   */
  public function getPluginId() {
    return $this->plugin_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setPluginId($id) {
    $this->plugin_id = $id;
  }

  /**
   * {@inheritdoc}
   */
  public function getOptions() {
    return $this->options;
  }

  /**
   * {@inheritdoc}
   */
  public function setOptions(array $options) {
    $this->options = $options;
  }

  /**
   * {@inheritdoc}
   */
  public function getOption($option) {
    return $this->options[$option] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setOption($option, $value) {
    $this->options[$option] = $value;
  }

}
