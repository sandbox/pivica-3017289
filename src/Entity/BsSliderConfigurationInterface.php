<?php

namespace Drupal\bs_slider\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining BS Slider entities.
 */
interface BsSliderConfigurationInterface extends ConfigEntityInterface {

  /**
   * Get BS Slider type.
   *
   * @return string
   *   BS Slider type.
   */
  public function getPluginId();

  /**
   * Set BS Slider type.
   *
   * @param string $id
   *   BS Slider type.
   */
  public function setPluginId(string $id);

  /**
   * Get options.
   *
   * @return array
   *   Options array.
   */
  public function getOptions();

  /**
   * Set options.
   *
   * @param array $options
   *   Options array.
   */
  public function setOptions(array $options);

  /**
   * Get specific option.
   *
   * @param string $option
   * @return mixed
   *   Option value if it exists or NULL.
   */
  public function getOption(string $option);

  /**
   * Set specific option.
   *
   * @param string $option
   *   Option name.
   * @param mixed $value
   *   Option value.
   */
  public function setOption($option, $value);

}
