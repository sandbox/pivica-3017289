CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Features
 * Configuration
 * Todo
 * Maintainers


INTRODUCTION
------------

Experimental plug-able slider, carousel and gallery framework implementation.


REQUIREMENTS
------------

This module does nothing on its own. You need to enable at least
bs_slider_bootstrap submodule in order to get some functionality.

The bs_slider_bootstrap requires the following modules:

 * BS Lib (https://drupal.org/project/bs_lib)

The bs_slider_entity_reference_revisions requires the following modules:

* Entity Reference Revision (https://www.drupal.org/project/entity_reference_revisions)


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.


FEATURES
--------

- Plug-able slider/gallery code base.
- Multiple configuration options set support.
- Built-in plugins:
  * Bootstrap Carousel. All features from Bootstrap Carousel are supported.
  * Bootstrap Gallery Grid. Two layout options: grid and column. Gutter option.
    Number of columns option.
- Couple of use-full field formatters:
  * Text Formatter. Use it when you want to create slider/carousel with text
    items.
  * Media Formatter. Use it when you want to create slider/carousel with media
    items. Anything that your media entities support will work: images, video,
    twitter items etc.
  * Media Gallery Formatter - use it when you want to create a gallery with
    thumbnails and full gallery view.
  * Entity Reference Revision Formatter - @todo.
- Basic bs_slider template with proper template suggestion and inheritance.


CONFIGURATION
-------------

@TODO


## DEVELOPMENT

Compile SASS using sass command like:

```shell script
sassc -t compressed  modules/bs_slider_bootstrap/css/gallery.scss >  modules/bs_slider_bootstrap/css/gallery.css
```


TODO
----

- Caption support for media formatter.
- Test coverage.
- File and image formatters?


MAINTAINERS
-----------

Current maintainers:
 * Ivica Puljic (pivica) - https://www.drupal.org/u/pivica
