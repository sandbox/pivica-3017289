<?php

/**
 * @file
 */

/**
 * Prepares variables for views bs slider rows templates.
 *
 * Default template: views-view-bs-slider.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - view: The view object.
 *   - rows: An array of row items. Each row is an array of content.
 */
function template_preprocess_views_view_bs_slider(&$variables) {
  // This works similar as views_view_unformatted, so we will reuse its
  // preprocessor.
  template_preprocess_views_view_unformatted($variables);

  $style_options = $variables['view']->getDisplay()->getOption('style');
  $bs_slider_type = $style_options['options']['bs_slider'];
  if (empty($bs_slider_type)) {
    return;
  }

  /** @var \Drupal\bs_slider\BsSliderConfigurationManagerInterface $manager */
  $manager = \Drupal::service('bs_slider_configuration.manager');

  /** @var \Drupal\bs_slider\Entity\BsSliderConfigurationInterface $bs_slider */
  $bs_slider = $manager->entityLoad($bs_slider_type);

  /** @var \Drupal\bs_slider\Plugin\BsSliderBase $plugin */
  if ($plugin = $manager->getPlugin($bs_slider_type)) {
    $plugin->view($variables['rows'], $bs_slider);
  }
}
