<?php

namespace Drupal\bs_slider_views\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;

/**
 * Style plugin to render rows in a bs slider.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "bs_slider_views",
 *   title = @Translation("BS Slider"),
 *   help = @Translation("Displays rows in a bs slider."),
 *   theme = "views_view_bs_slider",
 *   display_types = {"normal"}
 * )
 */
class BsSliderViews extends StylePluginBase {

  /**
   * {@inheritdoc}
   */
  protected $usesRowPlugin = TRUE;

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['bs_slider'] = ['default' => FALSE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $form['bs_slider'] = [
      '#type' => 'select',
      '#title' => $this->t('BS Slider'),
      '#options' => \Drupal::service('bs_slider_configuration.manager')->getAllOptionSet(),
      '#default_value' => $this->options['bs_slider'],
      '#empty_option' => $this->t('- None -'),
    ];
  }

}
