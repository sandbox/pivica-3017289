#!/usr/bin/env bash
# Run this from bs_slider_bootstrap folder.
cd css
sass-dart --no-source-map gallery.scss gallery.css; sass-dart --no-source-map layout.scss layout.css
cd ..
