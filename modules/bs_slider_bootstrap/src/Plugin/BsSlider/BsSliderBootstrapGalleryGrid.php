<?php

namespace Drupal\bs_slider_bootstrap\Plugin\BsSlider;

use Drupal\bs_slider\BsSliderPluginOptionInterface;
use Drupal\bs_slider\Entity\BsSliderConfigurationInterface;
use Drupal\bs_slider\Plugin\BsSliderBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

/**
 * Bootstrap Simple Gallery plugin.
 *
 * @BsSlider(
 *   id = "bootstrap_gallery_grid",
 *   label = @Translation("Bootstrap Gallery Grid"),
 *   description = @Translation("Simple gallery with column or grid layout list view and Bootstrap Carousel for a full view."),
 * )
 */
class BsSliderBootstrapGalleryGrid extends BsSliderBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'layout' => 'grid',
      'column_count' => '2-3-4',
      'gutter' => 'medium',
      'bs_slider_id' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $options = $this->getConfiguration();
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['options']['layout'] = [
      '#type' => 'select',
      '#title' => t('Layout type'),
      '#default_value' =>  $options['layout'],
      '#options' => [
        'grid' => t('Grid'),
        'column' => t('Column'),
      ],
    ];

    $form['options']['column_count'] = [
      '#type' => 'select',
      '#title' => t('Column count'),
      '#options' => ['1-2-2' => '1-2-2', '1-3-3' => '1-3-3', '1-2-4' => '1-2-4', '2-3-4' => '2-3-4', '2-3-6' => '2-3-6', '3-4-4' => '3-4-4', '3-6-9' => '3-6-9', '3-6-12' => '3-6-12'],
      '#description' => t('Number of thumbnails in a row for a screen. For example, option 1-2-2 means 1 cols for small screens, 2 cols for medium screen size and 2 cols for big screen size.'),
      '#default_value' => $options['column_count'],
    ];

    $form['options']['gutter'] = [
      '#type' => 'select',
      '#title' => t('Gutter size'),
      '#default_value' =>  $options['gutter'],
      '#options' => [
        'none' => t('None'),
        'small' => t('Small'),
        'medium' => t('Medium'),
        'large' => t('Large'),
      ],
    ];

    /** @var \Drupal\bs_slider\BsSliderConfigurationManager $manager */
    $manager = \Drupal::service('bs_slider_configuration.manager');
    $configurations = $manager->getAllOptionSetByProperties(['plugin_id' => 'bootstrap_carousel']);
    $form['options']['bs_slider_id'] = [
      '#type' => 'select',
      '#title' => t('BS Slider Configuration'),
      '#required' => TRUE,
      '#options' => $configurations,
      '#default_value' => $options['bs_slider_id'],
      '#description' => t('BS Slider configuration of the bootstrap_carousel type used for gallery full view.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildPluginOptionsForm(array $form, FormStateInterface $form_state, BsSliderPluginOptionInterface $plugin_option) {
    if (empty($plugin_option->getPluginOptions('target_field_view_modes'))) {
      return [];
    }

    $elements = parent::buildPluginOptionsForm($form, $form_state, $plugin_option);

    $elements['thumbnail_view_mode'] = [
      '#type' => 'select',
      '#options' => $plugin_option->getPluginOptions('target_field_view_modes'),
      '#title' => t('Thumbnail view mode'),
      '#default_value' => $plugin_option->getPluginOptionValue('thumbnail_view_mode'),
      '#required' => TRUE,
    ];

    $elements['item_view_mode'] = [
      '#type' => 'select',
      '#options' => $plugin_option->getPluginOptions('target_field_view_modes'),
      '#title' => t('Full view mode'),
      '#default_value' => $plugin_option->getPluginOptionValue('item_view_mode'),
      '#required' => TRUE,
    ];

    // @todo caption field selection.
    //$elements['info'] = ['#markup' => '@todo caption field selection.'];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function preprocess(array &$variables) {
    parent::preprocess($variables);

    $variables['id'] = $variables['element']['#id'];
    $variables['options'] = $this->getConfiguration();
    $variables['items'] = $variables['element']['#items'];
    $variables['gallery_items'] = $variables['element']['gallery_items'];
  }

  /**
   * {@inheritdoc}
   */
  public function view(array &$build, BsSliderConfigurationInterface $bs_slider, array $options = []) {
    $thumbnail_items = [];
    $gallery_items = [];
    foreach (Element::children($build) as $delta) {
      $thumbnail_items[$delta] = $build[$delta];
      $gallery_items[$delta] = $build[$delta];

      if (!empty($build[$delta]['#view_mode'])) {
        $thumbnail_items[$delta]['#view_mode'] = $options['thumbnail_view_mode'];
        $gallery_items[$delta]['#view_mode'] = $options['item_view_mode'];

        // Changing view_mode cache key value we will invalidate cache tag
        // for this item and will trigger rendering if target view mode is
        // different.
        if (isset($build[$delta]['#cache']['keys'][3])) {
          $thumbnail_items[$delta]['#cache']['keys'][3] = $options['thumbnail_view_mode'];
          $gallery_items[$delta]['#cache']['keys'][3] = $options['item_view_mode'];
        }
      }
    }

    // Thumbnail items.
    $build = $this->buildSliderArray($thumbnail_items, $bs_slider);

    // Gallery items.
    /** @var \Drupal\bs_slider\BsSliderConfigurationManagerInterface $manager */
    $manager = \Drupal::service('bs_slider_configuration.manager');
    /** @var \Drupal\bs_slider\Entity\BsSliderConfigurationInterface $bs_slider_gallery */
    $bs_slider_gallery = $manager->entityLoad($bs_slider->getOption('bs_slider_id'));
    $build['gallery_items'] = $this->buildSliderArray($gallery_items, $bs_slider_gallery);
  }

}
