<?php

namespace Drupal\bs_slider_bootstrap\Plugin\BsSlider;

use Drupal\bs_slider\BsSliderPluginOptionInterface;
use Drupal\bs_slider\Plugin\BsSliderBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Bootstrap Carousel plugin.
 *
 * @BsSlider(
 *   id = "bootstrap_carousel",
 *   label = @Translation("Bootstrap Carousel"),
 *   description = @Translation("Use Bootstrap Carousel as a slider."),
 * )
 */
class BsSliderBootstrapCarousel extends BsSliderBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'controls' => FALSE,
      'indicators' => FALSE,
      'captions' => FALSE,
      'crossfade' => FALSE,
      'interval' => 5000,
      'keyboard' => TRUE,
      'pause' => 'hover',
      'ride' => 'carousel',
      'wrap' => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $options = $this->getConfiguration();
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['options']['controls'] = [
      '#type' => 'checkbox',
      '#title' => t('Show previous and next controls'),
      '#default_value' =>  $options['controls'],
    ];

    $form['options']['indicators'] = [
      '#type' => 'checkbox',
      '#title' => t('Show indicators'),
      '#default_value' => $options['indicators'],
    ];

    $form['options']['captions'] = [
      '#type' => 'checkbox',
      '#title' => t('Show captions to your slides'),
      '#description' => t('This requires later defining a field source from which content will be used for captions.'),
      '#default_value' => $options['captions'],
    ];

    $form['options']['crossfade'] = [
      '#type' => 'checkbox',
      '#title' => t('Animate slides with a fade transition instead of a slide'),
      '#default_value' => $options['crossfade'],
    ];

    $form['options']['keyboard'] = [
      '#type' => 'checkbox',
      '#title' => t('Carousel react to keyboard events'),
      '#default_value' => $options['keyboard'],
    ];

    $form['options']['pause'] = [
      '#type' => 'checkbox',
      '#title' => t('Pauses the cycling of the carousel on hover or touch.'),
      '#default_value' => $options['pause'],
    ];

    $form['options']['ride'] = [
      '#type' => 'select',
      '#title' => t('Ride'),
      '#description' => t('Autoplays the carousel after the user manually cycles the first item. If "carousel", autoplays the carousel on load. It seems there is a bug in Bootstrap carousel documentation about this feature. Currently there is no autoplay after first manual cycle, see <a href="@issue-link">github issue</a>.', ['@issue-link' => 'https://github.com/twbs/bootstrap/issues/20971']),
      '#options' => [
        'false' => t('No'),
        'true' => t('Yes'),
        'carousel' => t('Carousel'),
      ],
      '#default_value' => $options['ride'],
    ];

    $form['options']['interval'] = [
      '#type' => 'number',
      '#min' => 0,
      '#title' => t('Interval'),
      '#description' => t('The amount of time (in milliseconds) to delay between automatically cycling an item. If 0, carousel will not automatically cycle.'),
      '#default_value' => $options['interval'],
      '#states' => [
        'visible' => ['#edit-options-ride' => ['value' => 'carousel']],
      ],
    ];

    $form['options']['wrap'] = [
      '#type' => 'checkbox',
      '#title' => t('Wrap carousel. If turned on when reached the end of the carousel go back to beginning.'),
      '#default_value' => $options['wrap'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildPluginOptionsForm(array $form, FormStateInterface $form_state, BsSliderPluginOptionInterface $plugin_option) {
    if (empty($plugin_option->getPluginOptions('target_field_view_modes'))) {
      return [];
    }

    $elements = parent::buildPluginOptionsForm($form, $form_state, $plugin_option);

    $elements['view_mode'] = [
      '#type' => 'select',
      '#options' => $plugin_option->getPluginOptions('target_field_view_modes'),
      '#title' => t('View mode'),
      '#default_value' => $plugin_option->getPluginOptionValue('view_mode'),
      '#required' => TRUE,
    ];

    // @todo
    //$elements['info'] = ['#markup' => '@todo caption field selection.'];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function preprocess(array &$variables) {
    parent::preprocess($variables);

    $options = $this->getConfiguration();

    $variables['id'] = $variables['element']['#id'];
    $variables['items'] = $variables['element']['#items'];
    $variables['options'] = $options;

    $variables['attributes'] += [
      'id' => $variables['id'],
      'data-interval' => $options['interval'],
      'data-keyboard' => $options['keyboard'] ? 'true' : 'false',
      'data-pause' => $options['pause'] ? 'hover' : 'false',
      'data-ride' => $options['ride'],
      'data-wrap' => $options['wrap'] ? 'true' : 'false',
    ];
  }

}
