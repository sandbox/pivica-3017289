<?php

namespace Drupal\bs_slider_entity_reference_revision\Plugin\Field\FieldFormatter;

use Drupal\bs_slider\BsSliderConfigurationManagerInterface;
use Drupal\bs_slider\Plugin\Field\FieldFormatter\BsSliderFormatterTrait;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\entity_reference_revisions\Plugin\Field\FieldFormatter\EntityReferenceRevisionsEntityFormatter;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'entity_reference_revisions' formatter.
 *
 * @FieldFormatter(
 *   id = "bs_slider_entity_reference_revisions",
 *   label = @Translation("BS Slider"),
 *   field_types = {
 *     "entity_reference_revisions"
 *   }
 * )
 */
class BsSliderEntityReferenceRevisionFormatter extends EntityReferenceRevisionsEntityFormatter {

  use BsSliderFormatterTrait;

  /**
   * Constructs a StringFormatter instance.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository.
   * @param \Drupal\bs_slider\BsSliderConfigurationManagerInterface $manager
   *   BS slider manager.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, LoggerChannelFactoryInterface $logger_factory, EntityDisplayRepositoryInterface $entity_display_repository, BsSliderConfigurationManagerInterface $manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings, $logger_factory, $entity_display_repository);
    $this->manager = $manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('logger.factory'),
      $container->get('entity_display.repository'),
      $container->get('bs_slider_configuration.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'bs_slider' => 'default',
        'link' => FALSE,
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements += $this->getSettingsFormElements($form, $form_state);

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    if ($bs_slider = $this->manager->entityLoad($this->getSetting('bs_slider'))) {
      $summary[] = $this->t('BS Slider: @name', ['@name' => $bs_slider->label()]);
    }
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    // Early opt-out if the field is empty.
    if ($items->isEmpty()) {
      return [];
    }

    $build = parent::viewElements($items, $langcode);
    $options = ['view_mode' => $this->viewMode];

    $plugin = $this->manager->getPlugin($this->getSetting('bs_slider'));

    $bs_slider = $this->manager->entityLoad($this->getSetting('bs_slider'));

    $plugin->view($build, $bs_slider, $options);

    return $build;
  }

}
