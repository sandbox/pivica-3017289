<?php

namespace Drupal\bs_slider_tiny_slider\Plugin\BsSlider;

use Drupal\bs_slider\BsSliderPluginOptionInterface;
use Drupal\bs_slider\Entity\BsSliderConfigurationInterface;
use Drupal\bs_slider\Plugin\BsSliderBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Tine Slider plugin.
 *
 * @BsSlider(
 *   id = "tiny_slider",
 *   label = @Translation("Tiny Slider"),
 *   description = @Translation("Use Tiny Slider as a slider."),
 * )
 */
class BsSliderTinySlider extends BsSliderBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'configuration' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $options = $this->getConfiguration();
    $form['options']['configuration'] = [
      '#type' => 'textarea',
      '#title' => t('Configuration'),
      '#default_value' =>  $options['configuration'],
      '#rows' => 15,
      '#description' => t('Tiny Slider configuration in YAML format. For possible options check <a href="https://github.com/ganlanyuan/tiny-slider#options">documentation</a>. Do not use <em>container</em> option, this is handled by module itself.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildPluginOptionsForm(array $form, FormStateInterface $form_state, BsSliderPluginOptionInterface $plugin_option) {
    if (empty($plugin_option->getPluginOptions('target_field_view_modes'))) {
      return [];
    }

    $elements = parent::buildPluginOptionsForm($form, $form_state, $plugin_option);

    $elements['view_mode'] = [
      '#type' => 'select',
      '#options' => $plugin_option->getPluginOptions('target_field_view_modes'),
      '#title' => t('View mode'),
      '#default_value' => $plugin_option->getPluginOptionValue('view_mode'),
      '#required' => TRUE,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function preprocess(&$variables) {
    parent::preprocess($variables);

    $variables['items'] = $variables['element']['#items'];
    $variables['attributes']['id'] = $variables['element']['#id'];
    $variables['attributes']['data-bs-slider'] = 'tiny-slider';

    $options = $this->getConfiguration();
    $variables['attributes']['data-bs-slider-options'] = json_encode(Yaml::parse($options['configuration']));
  }

  /**
   * {@inheritdoc}
   */
  public function view(array &$build, BsSliderConfigurationInterface $bs_slider, array $options = []) {
    parent::view($build, $bs_slider, $options);
    $build['#attached'] = ['library' => ['bs_slider_tiny_slider/tiny-slider']];
  }

}
