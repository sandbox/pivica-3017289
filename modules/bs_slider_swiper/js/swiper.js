/**
 * @file
 * BS Slider Swiper JS functionality.
 */
(function (Drupal, Swiper) {

  'use strict';

  /**
   * bsSliderSwiper behaviour.
   */
  Drupal.behaviors.bsSliderSwiper = {
    attach: function (context, settings) {
      // If context is gallery itself we need to switch to context parent.
      let _context = context;
      if (_context.dataset && _context.dataset.bsSlider === 'swiper') {
        _context = context.parentNode;
      }

      let sliders = _context.querySelectorAll('[data-bs-slider="swiper"]');
      sliders.forEach(function (slider) {
        if (!slider.dataset.bsSliderInit) {
          // Init slider and mark is as initialized.
          slider.dataset.bsSliderInit = true;
          const options = JSON.parse(slider.dataset.bsSliderOptions);

          // For navigation, pagination and scrollbar modules we need to
          // convert css selectors to elements in order to avoid problems with
          // multiple sliders on the same page.
          if (options.navigation) {
            if (options.navigation.nextEl && typeof options.navigation.nextEl === 'string') {
              options.navigation.nextEl = slider.querySelector(options.navigation.nextEl);
            }
            if (options.navigation.prevEl && typeof options.navigation.prevEl === 'string') {
              options.navigation.prevEl = slider.querySelector(options.navigation.prevEl);
            }
          }
          if (options.pagination && options.pagination.el && typeof options.pagination.el === 'string') {
            options.pagination.el = slider.querySelector(options.pagination.el);
          }
          if (options.scrollbar && options.scrollbar.el && typeof options.scrollbar.el === 'string') {
            options.scrollbar.el = slider.querySelector(options.scrollbar.el);
          }

          // Thumbs support.
          if (options.hasOwnProperty('_thumbs_id')) {
            let thumbsSwiper;
            const thumbsSlider = document.querySelector('#' + options._thumbs_id);
            // If thumbs slider is not initialized we need to do that first.
            if (!thumbsSlider.dataset.bsSliderInit) {
              thumbsSlider.dataset.bsSliderInit = true;
              thumbsSwiper = new Swiper(thumbsSlider, JSON.parse(thumbsSlider.dataset.bsSliderOptions));
            }
            else {
              thumbsSwiper = thumbsSwiper.swiper;
            }

            // Assign thumbs swiper to this swiper.
            options.thumbs = {
              swiper: thumbsSwiper
            };
          }

          // Fullscreen support.
          if (options._fullscreen) {
            // TODO - this needs to be tested against item with links!
            let parentContainer = slider;
            if (options._thumbs_id) {
              parentContainer = slider.parentElement;
            }
            const items = slider.querySelectorAll('.swiper-slide');
            items.forEach(function (item) {
              item.addEventListener('click', (e) => {
                if (!document.fullscreenElement) {
                  parentContainer.requestFullscreen();
                  parentContainer.classList.add('bs-slider--fullscreen');
                } else {
                  document.exitFullscreen();
                  parentContainer.classList.remove('bs-slider--fullscreen');
                }
                e.stopPropagation();
              });
            });
          }

          const swiper = new Swiper(slider, options);
        }
      });
    }
  };

})(Drupal, Swiper);
