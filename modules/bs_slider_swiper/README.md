CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Features
 * Configuration
 * Todo
 * Maintainers


INTRODUCTION
------------

This modules integrate [Swiper](https://github.com/nolimits4web/swiper)
javascript library.


REQUIREMENTS
------------

You need to download Swiper library. This module has been tested with
version 9.1.1. After download extract files to libraries/swiper.

In case you are using composer for your project (recommended) with
composer/installers then execute next commands:

```shell script
composer config repositories.swiper '{"type": "package", "package": {"name": "nolimits4web/swiper", "version": "9.1.1", "type": "drupal-library", "dist": {"url": "https://registry.npmjs.org/swiper/-/swiper-9.1.1.tgz", "type": "tar"}';
composer require nolimits4web/swiper:9.1.1;
```


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.


FEATURES
--------

@TODO


CONFIGURATION
-------------

@TODO


MAINTAINERS
-----------

Current maintainers:
 * Ivica Puljic (pivica) - https://www.drupal.org/u/pivica
