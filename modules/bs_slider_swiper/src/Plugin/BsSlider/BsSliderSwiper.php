<?php

namespace Drupal\bs_slider_swiper\Plugin\BsSlider;

use Drupal\bs_slider\BsSliderPluginOptionInterface;
use Drupal\bs_slider\Entity\BsSliderConfigurationInterface;
use Drupal\bs_slider\Plugin\BsSliderBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\Yaml\Yaml;

// TODO
// - Check with somebody how to define config schema for Swiper because there
//   are huge amount of options?
// - Accessibility module options
// - Hash Navigation module options
// - History Navigation module options
// - Virtual Slides module options
// - Mousewheel Control module options
// - Zoom module options
// - Creative Effect module options
// - Parallax module options

/**
 * Swiper plugin.
 *
 * @BsSlider(
 *   id = "swiper",
 *   label = @Translation("Swiper"),
 *   description = @Translation("Use Swiper as a slider."),
 * )
 */
class BsSliderSwiper extends BsSliderBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    // NOTE - values of options MUST be the same as default options values on
    // https://swiperjs.com/swiper-api#parameters so cleanup of options in
    // preprocess can work correctly.
    return [
      // Custom parameters, with _ prefix.
      '_advanced_configuration' => '',
      '_lazyLoading' => FALSE,
      '_fullscreen' => FALSE,
      '_show_advanced_options' => FALSE,
      // Parameters.
      'allowTouchMove' => TRUE,
      'autoHeight' => FALSE,
      'breakpoints' => NULL,
      'centerInsufficientSlides' => FALSE,
      'centeredSlides' => FALSE,
      'centeredSlidesBounds' => FALSE,
      'cssMode' => FALSE,
      'direction' => 'horizontal',
      'grabCursor' => FALSE,
      'initialSlide' => 0,
      'lazyPreloadPrevNext' => 0,
      'longSwipes' => TRUE,
      'loop' => FALSE,
      'rewind' => FALSE,
      'simulateTouch' => TRUE,
      'slidesOffsetAfter' => 0,
      'slidesOffsetBefore' => 0,
      'slidesPerView' => 1,
      'slidesPerGroup' => 1,
      'spaceBetween' => 0,
      'speed' => 300,
      'touchReleaseOnEdges' => FALSE,
      'watchOverflow' => TRUE,
      'watchSlidesProgress' => FALSE,
      // Modules.
      'navigation' => [
        'enabled' => FALSE,
        'nextEl' => '.swiper-button-next',
        'prevEl' => '.swiper-button-prev',
      ],
      'pagination' => [
        'enabled' => FALSE,
        'type' => 'bullets',
        'clickable' => FALSE,
        'dynamicBullets' => FALSE,
        'dynamicMainBullets' => 1,
        'el' => '.swiper-pagination',
      ],
      'scrollbar' => [
        'enabled' => FALSE,
        'draggable' => FALSE,
        'dragSize' => 'auto',
        'el' => '.swiper-scrollbar',
        'snapOnRelease' => FALSE,
      ],
      'autoplay' => [
        'enabled' => FALSE,
        'delay' => 3000,
        'disableOnInteraction' => TRUE,
        'pauseOnMouseEnter' => FALSE,
        'stopOnLastSlide' => FALSE,
        'waitForTransition' => TRUE,
      ],
      'freeMode' => [
        'enabled' => FALSE,
        'sticky' => FALSE,
      ],
      'keyboard' => [
        'enabled' => FALSE,
        'onlyInViewport' => TRUE,
        'pageUpDown' => TRUE,
      ],
      'effect' => 'slide',
      'fadeEffect' => [
        'crossFade' => FALSE,
      ],
      'coverflowEffect' => [
        'depth' => 100,
        'modifier' => 1,
        'rotate' => 50,
        'scale' => 1,
        'slideShadows' => TRUE,
        'stretch' => 0
      ],
      'flipEffect' => [
        'limitRotation' => TRUE,
        'slideShadows' => TRUE,
      ],
      'cubeEffect' => [
        'shadow' => TRUE,
        'shadowOffset' => 20,
        'shadowScale' => 0.94,
        'slideShadows' => TRUE,
      ],
      'cardsEffect' => [
        'perSlideOffset' => 8,
        'perSlideRotate' => 2,
        'rotate' => TRUE,
        'slideShadows' => TRUE,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $options = $this->getConfiguration();

    $form['options']['_show_advanced_options'] = [
      '#type' => 'checkbox',
      '#title' => t('Show advanced options'),
      '#default_value' => $options['_show_advanced_options'],
    ];

    $form['options']['allowTouchMove'] = [
      '#type' => 'checkbox',
      '#title' => t('Allow touch move'),
      '#description' => t('If disabled, then the only way to switch the slide is use of external API functions like slidePrev or slideNext.'),
      '#default_value' => $options['allowTouchMove'],
      '#states' => ['visible' => ['[name="options[_show_advanced_options]"]' => ['checked' => TRUE]]],
    ];

    $form['options']['autoHeight'] = [
      '#type' => 'checkbox',
      '#title' => t('Auto height'),
      '#description' => t('Slider wrapper will adapt its height to the height of the currently active slide.'),
      '#default_value' => $options['autoHeight'],
      '#states' => ['visible' => ['[name="options[_show_advanced_options]"]' => ['checked' => TRUE]]],
    ];

    $form['options']['centerInsufficientSlides'] = [
      '#type' => 'checkbox',
      '#title' => t('Center insufficient slides'),
      '#description' => t('Center slides if the amount of slides less than slidesPerView. Not intended to be used with loop mode and grid.rows.'),
      '#default_value' => $options['centerInsufficientSlides'],
    ];

    $form['options']['centeredSlides'] = [
      '#type' => 'checkbox',
      '#title' => t('Centered slides'),
      '#description' => t('Active slide will be centered, not always on the left side.'),
      '#default_value' => $options['centeredSlides'],
    ];

    $form['options']['centeredSlidesBounds'] = [
      '#type' => 'checkbox',
      '#title' => t('Centered slides bounds'),
      '#description' => t('Active slide will be centered without adding gaps at the beginning and end of slider. Required centeredSlides. Not intended to be used with loop or pagination.'),
      '#default_value' => $options['centeredSlidesBounds'],
      '#states' => ['visible' => ['[name="options[_show_advanced_options]"]' => ['checked' => TRUE]]],
    ];

    $form['options']['cssMode'] = [
      '#type' => 'checkbox',
      '#title' => t('CSS mode'),
      '#description' => t('When enabled it will use modern CSS Scroll Snap API. It doesn\'t support all of Swiper\'s features, but potentially should bring a much better performance in simple configurations..'),
      '#default_value' => $options['cssMode'],
      '#states' => ['visible' => ['[name="options[_show_advanced_options]"]' => ['checked' => TRUE]]],
    ];

    $form['options']['direction'] = [
      '#type' => 'select',
      '#title' => t('Direction'),
      '#options' => ['horizontal' => t('Horizontal'), 'vertical' => t('Vertical')],
      '#default_value' => $options['direction'],
    ];

    $form['options']['grabCursor'] = [
      '#type' => 'checkbox',
      '#title' => t('Grab cursor'),
      '#description' => t('This option may a little improve desktop usability. If checked, user will see the "grab" cursor when hover on Swiper.'),
      '#default_value' => $options['grabCursor'],
    ];

    $form['options']['_fullscreen'] = [
      '#type' => 'checkbox',
      '#title' => t('Fullscreen support'),
      '#description' => t('Clicking on slider item will switch slider to full screen. In fullscreen clicking on slider item will exit from fullscreen mode. Works also with thumbs galleries.'),
      '#default_value' => $options['_fullscreen'],
    ];

    $form['options']['initialSlide'] = [
      '#type' => 'number',
      '#title' => t('Initial slide'),
      '#min' => 0,
      '#description' => t('Index number of initial slide, starting from 0.'),
      '#default_value' => $options['initialSlide'],
    ];

    $form['options']['_lazyLoading'] = [
      '#type' => 'checkbox',
      '#title' => t('Lazy loading'),
      '#description' => t('Since version 9 Swiper doesn\'t have a specific lazy loading API, as it relies on native browser lazy loading feature. To use lazy loading, make sure you set loading="lazy" on images.'),
      '#default_value' => $options['_lazyLoading'],
    ];
    $form['options']['lazyPreloadPrevNext'] = [
      '#type' => 'number',
      '#title' => t('Lazy preload prev/next'),
      '#min' => 0,
      '#description' => t('Number of next and previous slides to preload.'),
      '#default_value' => $options['lazyPreloadPrevNext'],
      '#states' => ['visible' => ['[name="options[_lazyLoading]"]' => ['checked' => TRUE]]],
    ];

    $form['options']['longSwipes'] = [
      '#type' => 'checkbox',
      '#title' => t('Long swipes'),
      '#description' => t('Uncheck if you want to disable long swipes.'),
      '#default_value' => $options['longSwipes'],
      '#states' => ['visible' => ['[name="options[_show_advanced_options]"]' => ['checked' => TRUE]]],
    ];

    $form['options']['loop'] = [
      '#type' => 'checkbox',
      '#title' => t('Loop'),
      '#description' => t('Enable continuous loop mode. Because of nature of how the loop mode works (it will rearrange slides), total number of slides must be >= slidesPerView * 2'),
      '#default_value' => $options['loop'],
    ];

    $form['options']['rewind'] = [
      '#type' => 'checkbox',
      '#title' => t('Rewind'),
      '#description' => t('When enabled, clicking "next" navigation button when on last slide will slide back to the first slide. Clicking "prev" navigation button when on first slide will slide forward to the last slide. Should not be used together with loop mode.'),
      '#default_value' => $options['rewind'],
    ];

    $form['options']['simulateTouch'] = [
      '#type' => 'checkbox',
      '#title' => t('Simulate touch'),
      '#description' => t('Swiper will accept mouse events like touch events (click and drag to change slides).'),
      '#default_value' => $options['simulateTouch'],
      '#states' => ['visible' => ['[name="options[_show_advanced_options]"]' => ['checked' => TRUE]]],
    ];

    $form['options']['slidesOffsetAfter'] = [
      '#type' => 'number',
      '#title' => t('Slides offset after'),
      '#description' => t('Add (in px) additional slide offset in the end of the container (after all slides).'),
      '#min' => 0,
      '#default_value' => $options['slidesOffsetAfter'],
      '#states' => ['visible' => ['[name="options[_show_advanced_options]"]' => ['checked' => TRUE]]],
    ];

    $form['options']['slidesOffsetBefore'] = [
      '#type' => 'number',
      '#title' => t('Slides offset before'),
      '#description' => t('Add (in px) additional slide offset in the beginning of the container (before all slides).'),
      '#default_value' => $options['slidesOffsetBefore'],
      '#min' => 0,
      '#states' => ['visible' => ['[name="options[_show_advanced_options]"]' => ['checked' => TRUE]]],
    ];

    $form['options']['slidesPerView'] = [
      '#type' => 'textfield',
      '#title' => t('Slides per view'),
      '#description' => t('Number of slides per view (slides visible at the same time on slider\'s container). \'auto\' is currently not compatible with multirow mode, when grid.rows > 1'),
      '#default_value' => $options['slidesPerView'],
    ];

    $form['options']['slidesPerGroup'] = [
      '#type' => 'number',
      '#title' => t('Slides per group'),
      '#description' => t('Set numbers of slides to define and enable group sliding. Useful to use with slidesPerView > 1'),
      '#default_value' => $options['slidesPerGroup'],
    ];

    $form['options']['spaceBetween'] = [
      '#type' => 'number',
      '#title' => t('Space between'),
      '#description' => t('Distance between slides in px.'),
      '#default_value' => $options['spaceBetween'],
      '#min' => 0,
    ];

    $form['options']['speed'] = [
      '#type' => 'number',
      '#title' => t('Speed'),
      '#description' => t('Duration of transition between slides (in ms).'),
      '#default_value' => $options['speed'],
      '#min' => 0,
    ];

    $form['options']['touchReleaseOnEdges'] = [
      '#type' => 'checkbox',
      '#title' => t('Touch release on edges'),
      '#description' => t('Release touch events on slider edge position (beginning, end) to allow for further page scrolling.'),
      '#default_value' => $options['touchReleaseOnEdges'],
      '#states' => ['visible' => ['[name="options[_show_advanced_options]"]' => ['checked' => TRUE]]],
    ];

    $form['options']['watchOverflow'] = [
      '#type' => 'checkbox',
      '#title' => t('Watch overflow'),
      '#description' => t('Swiper will be disabled and hide navigation buttons on case there are not enough slides for sliding.'),
      '#default_value' => $options['watchOverflow'],
      '#states' => ['visible' => ['[name="options[_show_advanced_options]"]' => ['checked' => TRUE]]],
    ];

    $form['options']['watchSlidesProgress'] = [
      '#type' => 'checkbox',
      '#title' => t('Watch slides progress'),
      '#description' => t('Enable this feature to calculate each slides progress and visibility (slides in viewport will have additional visible class).'),
      '#default_value' => $options['watchSlidesProgress'],
      '#states' => ['visible' => ['[name="options[_show_advanced_options]"]' => ['checked' => TRUE]]],
    ];

    // Modules.

    // Navigation.
    $form['options']['navigation'] = [
      '#type' => 'fieldset',
      '#title' => t('Navigation'),
      '#tree' => TRUE
    ];
    $form['options']['navigation']['enabled'] = [
      '#type' => 'checkbox',
      '#title' => t('Enable navigation'),
      '#default_value' => $options['navigation']['enabled'],
    ];
    $form['options']['navigation']['nextEl'] = [
      '#type' => 'textfield',
      '#title' => t('Next element'),
      '#description' => t('String with CSS selector or HTML element of the element that will work like "next" button after click on it.'),
      '#default_value' => $options['navigation']['nextEl'],
      '#states' => ['visible' => ['input[name="options[navigation][enabled]"]' => ['checked' => TRUE]]],
    ];
    $form['options']['navigation']['prevEl'] = [
      '#type' => 'textfield',
      '#title' => t('Previous element'),
      '#description' => t('String with CSS selector or HTML element of the element that will work like "previous" button after click on it.'),
      '#default_value' => $options['navigation']['prevEl'],
      '#states' => ['visible' => ['input[name="options[navigation][enabled]"]' => ['checked' => TRUE]]],
    ];

    // Pagination.
    $form['options']['pagination'] = [
      '#type' => 'fieldset',
      '#title' => t('Pagination'),
      '#tree' => TRUE
    ];
    $form['options']['pagination']['enabled'] = [
      '#type' => 'checkbox',
      '#title' => t('Enable pagination'),
      '#default_value' => $options['pagination']['enabled'],
    ];
    $form['options']['pagination']['type'] = [
      '#type' => 'select',
      '#title' => t('Type'),
      '#options' => [
        'bullets' => t('Bullets'),
        'fraction' => t('Fraction'),
        'progressbar' => t('Progress bar'),
        'custom' => t('Custom'),
      ],
      '#default_value' => $options['pagination']['type'],
      '#states' => ['visible' => ['input[name="options[pagination][enabled]"]' => ['checked' => TRUE]]],
    ];
    $form['options']['pagination']['el'] = [
      '#type' => 'textfield',
      '#title' => t('Element'),
      '#description' => t('String with CSS selector or HTML element of the container with pagination.'),
      '#default_value' => $options['pagination']['el'],
      '#states' => ['visible' => ['input[name="options[pagination][enabled]"]' => ['checked' => TRUE]]],
    ];
    $form['options']['pagination']['clickable'] = [
      '#type' => 'checkbox',
      '#title' => t('Clickable'),
      '#description' => t('Clicking on pagination button will cause transition to appropriate slide. Only for bullets pagination type.'),
      '#default_value' => $options['pagination']['clickable'],
      '#states' => ['visible' => ['input[name="options[pagination][enabled]"]' => ['checked' => TRUE]]],
    ];
    $form['options']['pagination']['dynamicBullets'] = [
      '#type' => 'checkbox',
      '#title' => t('Dynamic bullets'),
      '#description' => t('Good to enable if you use bullets pagination with a lot of slides. So it will keep only few bullets visible at the same time.'),
      '#default_value' => $options['pagination']['dynamicBullets'],
      '#states' => ['visible' => ['input[name="options[pagination][enabled]"]' => ['checked' => TRUE]]],
    ];
    $form['options']['pagination']['dynamicMainBullets'] = [
      '#type' => 'number',
      '#title' => t('Dynamic main bullets'),
      '#description' => t('The number of main bullets visible when dynamicBullets enabled.'),
      '#default_value' => $options['pagination']['dynamicMainBullets'],
      '#states' => ['visible' => ['input[name="options[pagination][enabled]"]' => ['checked' => TRUE]]],
    ];

    // Scrollbar.
    $form['options']['scrollbar'] = [
      '#type' => 'fieldset',
      '#title' => t('Scrollbar'),
      '#tree' => TRUE
    ];
    $form['options']['scrollbar']['enabled'] = [
      '#type' => 'checkbox',
      '#title' => t('Enable scrollbar'),
      '#default_value' => $options['scrollbar']['enabled'],
    ];
    $form['options']['scrollbar']['el'] = [
      '#type' => 'textfield',
      '#title' => t('Element'),
      '#description' => t('String with CSS selector or HTML element of the container with scrollbar.'),
      '#default_value' => $options['scrollbar']['el'],
      '#states' => ['visible' => ['input[name="options[scrollbar][enabled]"]' => ['checked' => TRUE]]],
    ];
    $form['options']['scrollbar']['draggable'] = [
      '#type' => 'checkbox',
      '#title' => t('Draggable'),
      '#description' => t('Enable make scrollbar draggable that allows you to control slider position.'),
      '#default_value' => $options['scrollbar']['draggable'],
      '#states' => ['visible' => ['input[name="options[scrollbar][enabled]"]' => ['checked' => TRUE]]],
    ];
    $form['options']['scrollbar']['dragSize'] = [
      '#type' => 'textfield',
      '#title' => t('Drag size'),
      '#description' => t('Size of scrollbar draggable element in px or <em>auto</em>.'),
      '#default_value' => $options['scrollbar']['dragSize'],
      '#states' => ['visible' => ['input[name="options[scrollbar][enabled]"]' => ['checked' => TRUE]]],
    ];
    $form['options']['scrollbar']['snapOnRelease'] = [
      '#type' => 'checkbox',
      '#title' => t('Snap on release'),
      '#description' => t('Snap slider position to slides when you release scrollbar.'),
      '#default_value' => $options['scrollbar']['snapOnRelease'],
      '#states' => ['visible' => ['input[name="options[scrollbar][enabled]"]' => ['checked' => TRUE]]],
    ];

    // Autoplay.
    $form['options']['autoplay'] = [
      '#type' => 'fieldset',
      '#title' => t('Autoplay'),
      '#tree' => TRUE,
    ];
    $form['options']['autoplay']['enabled'] = [
      '#type' => 'checkbox',
      '#title' => t('Enable autoplay'),
      '#default_value' => $options['autoplay']['enabled'],
    ];
    $form['options']['autoplay']['delay'] = [
      '#type' => 'number',
      '#title' => t('Delay'),
      '#description' => t('Delay between transitions (in ms).'),
      // Check if delay is set to avoid PHP undefined array key warning for
      // current installations.
      // TODO - remove in some later version.
      '#default_value' => $options['autoplay']['delay'] ?? 3000,
      '#min' => 0,
      '#states' => ['visible' => ['input[name="options[autoplay][enabled]"]' => ['checked' => TRUE]]],
    ];
    $form['options']['autoplay']['disableOnInteraction'] = [
      '#type' => 'checkbox',
      '#title' => t('Disable on interaction'),
      '#description' => t('If disabled autoplay will not be disabled after user interactions (swipes), it will be restarted every time after interaction.'),
      '#default_value' => $options['autoplay']['disableOnInteraction'],
      '#states' => ['visible' => ['input[name="options[autoplay][enabled]"]' => ['checked' => TRUE]]],
    ];
    $form['options']['autoplay']['pauseOnMouseEnter'] = [
      '#type' => 'checkbox',
      '#title' => t('Pause on mouse enter'),
      '#description' => t('When enabled autoplay will be paused on pointer (mouse) enter over Swiper container.'),
      '#default_value' => $options['autoplay']['pauseOnMouseEnter'],
      '#states' => ['visible' => ['input[name="options[autoplay][enabled]"]' => ['checked' => TRUE]]],
    ];
    $form['options']['autoplay']['stopOnLastSlide'] = [
      '#type' => 'checkbox',
      '#title' => t('Stop on last slide'),
      '#description' => t('Autoplay will be stopped when it reaches last slide (has no effect in loop mode).'),
      '#default_value' => $options['autoplay']['stopOnLastSlide'],
      '#states' => ['visible' => ['input[name="options[autoplay][enabled]"]' => ['checked' => TRUE]]],
    ];
    $form['options']['autoplay']['waitForTransition'] = [
      '#type' => 'checkbox',
      '#title' => t('Wait for transition'),
      '#description' => t('Autoplay will wait for wrapper transition to continue. Can be disabled in case of using Virtual Translate when your slider may not have transition.'),
      '#default_value' => $options['autoplay']['waitForTransition'],
      '#states' => ['visible' => ['input[name="options[autoplay][enabled]"]' => ['checked' => TRUE]]],
    ];

    // Free mode.
    $form['options']['freeMode'] = [
      '#type' => 'fieldset',
      '#title' => t('Free mode'),
      '#tree' => TRUE,
    ];
    $form['options']['freeMode']['enabled'] = [
      '#type' => 'checkbox',
      '#title' => t('Enable free mode'),
      '#default_value' => $options['freeMode']['enabled'],
    ];
    $form['options']['freeMode']['sticky'] = [
      '#type' => 'checkbox',
      '#title' => t('Sticky'),
      '#description' => t('Set to enabled to enable snap to slides positions in free mode.'),
      '#default_value' => $options['freeMode']['sticky'],
      '#states' => ['visible' => ['input[name="options[freeMode][enabled]"]' => ['checked' => TRUE]]],
    ];

    // Keyboard control.
    $form['options']['keyboard'] = [
      '#type' => 'fieldset',
      '#title' => t('Keyboard control'),
      '#tree' => TRUE,
    ];
    $form['options']['keyboard']['enabled'] = [
      '#type' => 'checkbox',
      '#title' => t('Enable keyboard control'),
      '#default_value' => $options['keyboard']['enabled'],
    ];
    $form['options']['keyboard']['onlyInViewport'] = [
      '#type' => 'checkbox',
      '#title' => t('Only in viewport'),
      '#description' => t('Control sliders that are currently in viewport.'),
      '#default_value' => $options['keyboard']['onlyInViewport'],
      '#states' => ['visible' => ['input[name="options[keyboard][enabled]"]' => ['checked' => TRUE]]],
    ];
    $form['options']['keyboard']['pageUpDown'] = [
      '#type' => 'checkbox',
      '#title' => t('Page up down'),
      '#description' => t('Enable keyboard navigation by Page Up and Page Down keys.'),
      '#default_value' => $options['keyboard']['pageUpDown'],
      '#states' => ['visible' => ['input[name="options[keyboard][enabled]"]' => ['checked' => TRUE]]],
    ];

    // Effects.
    $form['options']['effect'] = [
      '#type' => 'select',
      '#title' => t('Effect'),
      '#options' => [
        'cards' => t('Cards'),
        'coverflow' => t('Cover flow'),
        // TODO 'creative' => t('Creative'),
        'cube' => t('Cube'),
        'fade' => t('Fade'),
        'flip' => t('Flip'),
        'slide' => t('Slide'),
      ],
      '#default_value' => $options['effect'],
    ];

    $form['options']['fadeEffect'] = [
      '#type' => 'fieldset',
      '#title' => t('Fade effect'),
      '#tree' => TRUE,
      '#states' => ['visible' => ['[name="options[effect]"]' => ['value' => 'fade']]],
    ];
    $form['options']['fadeEffect']['crossFade'] = [
      '#type' => 'checkbox',
      '#title' => t('Cross fade'),
      '#description' => t('Enables slides cross fade. Note that crossFade should be set to true in order to avoid seeing content behind or underneath.'),
      '#default_value' => $options['fadeEffect']['crossFade'],
    ];

    $form['options']['coverflowEffect'] = [
      '#type' => 'fieldset',
      '#title' => t('Coverflow effect'),
      '#tree' => TRUE,
      '#states' => ['visible' => ['[name="options[effect]"]' => ['value' => 'coverflow']]],
    ];
    $form['options']['coverflowEffect']['depth'] = [
      '#type' => 'number',
      '#title' => t('Depth'),
      '#description' => t('Depth offset in px (slides translate in Z axis).'),
      '#default_value' => $options['coverflowEffect']['depth'],
      '#min' => 0,
    ];
    $form['options']['coverflowEffect']['modifier'] = [
      '#type' => 'number',
      '#title' => t('Modifier'),
      '#description' => t('Effect multiplier.'),
      '#default_value' => $options['coverflowEffect']['modifier'],
      '#min' => 0,
    ];
    $form['options']['coverflowEffect']['rotate'] = [
      '#type' => 'number',
      '#title' => t('Rotate'),
      '#description' => t('Slide rotate in degrees.'),
      '#default_value' => $options['coverflowEffect']['rotate'],
      '#min' => 0,
    ];
    $form['options']['coverflowEffect']['scale'] = [
      '#type' => 'number',
      '#title' => t('Scale'),
      '#description' => t('Slide scale effect.'),
      '#default_value' => $options['coverflowEffect']['scale'],
      '#min' => 0,
    ];
    $form['options']['coverflowEffect']['slideShadows'] = [
      '#type' => 'checkbox',
      '#title' => t('Slide shadows'),
      '#description' => t('Enables slides shadows.'),
      '#default_value' => $options['coverflowEffect']['slideShadows'],
    ];
    $form['options']['coverflowEffect']['stretch'] = [
      '#type' => 'number',
      '#title' => t('Stretch'),
      '#description' => t('Stretch space between slides (in px).'),
      '#default_value' => $options['coverflowEffect']['stretch'],
      '#min' => 0,
    ];

    $form['options']['flipEffect'] = [
      '#type' => 'fieldset',
      '#title' => t('Flip effect'),
      '#tree' => TRUE,
      '#states' => ['visible' => ['[name="options[effect]"]' => ['value' => 'flip']]],
    ];
    $form['options']['flipEffect']['limitRotation'] = [
      '#type' => 'checkbox',
      '#title' => t('Limit rotation'),
      '#description' => t('Limit edge slides rotation.'),
      '#default_value' => $options['flipEffect']['limitRotation'],
    ];
    $form['options']['flipEffect']['slideShadows'] = [
      '#type' => 'checkbox',
      '#title' => t('Slide shadows'),
      '#description' => t('Enables slides shadows.'),
      '#default_value' => $options['flipEffect']['slideShadows'],
    ];

    $form['options']['cubeEffect'] = [
      '#type' => 'fieldset',
      '#title' => t('Cube effect'),
      '#tree' => TRUE,
      '#states' => ['visible' => ['[name="options[effect]"]' => ['value' => 'cube']]],
    ];
    $form['options']['cubeEffect']['shadow'] = [
      '#type' => 'checkbox',
      '#title' => t('Shadow'),
      '#description' => t('Enables main slider shadow.'),
      '#default_value' => $options['cubeEffect']['shadow'],
    ];
    $form['options']['cubeEffect']['shadowOffset'] = [
      '#type' => 'number',
      '#title' => t('Shadow offset'),
      '#description' => t('Main shadow offset in px.'),
      '#default_value' => $options['cubeEffect']['shadowOffset'],
      '#min' => 0,
    ];
    $form['options']['cubeEffect']['shadowScale'] = [
      '#type' => 'textfield',
      '#title' => t('Shadow scale'),
      '#description' => t('Main shadow scale ratio.'),
      '#default_value' => $options['cubeEffect']['shadowScale'],
      '#min' => 0,
    ];
    $form['options']['cubeEffect']['slideShadows'] = [
      '#type' => 'checkbox',
      '#title' => t('Slide shadows'),
      '#description' => t('Enables slides shadows.'),
      '#default_value' => $options['cubeEffect']['slideShadows'],
    ];

    $form['options']['cardsEffect'] = [
      '#type' => 'fieldset',
      '#title' => t('Cards effect'),
      '#tree' => TRUE,
      '#states' => ['visible' => ['[name="options[effect]"]' => ['value' => 'cards']]],
    ];

    $form['options']['cardsEffect']['perSlideOffset'] = [
      '#type' => 'number',
      '#title' => t('Per slide offset'),
      '#description' => t('Offset distance per slide (in px).'),
      '#default_value' => $options['cardsEffect']['perSlideOffset'],
    ];
    $form['options']['cardsEffect']['perSlideRotate'] = [
      '#type' => 'number',
      '#title' => t('Per slide rotate'),
      '#description' => t('Rotate angle per slide (in degrees).'),
      '#default_value' => $options['cardsEffect']['perSlideRotate'],
    ];
    $form['options']['cardsEffect']['rotate'] = [
      '#type' => 'checkbox',
      '#title' => t('Rotate'),
      '#description' => t('Enables cards rotation.'),
      '#default_value' => $options['cardsEffect']['rotate'],
    ];
    $form['options']['cardsEffect']['slideShadows'] = [
      '#type' => 'checkbox',
      '#title' => t('Slide shadows'),
      '#description' => t('Enables slides shadows.'),
      '#default_value' => $options['cardsEffect']['slideShadows'],
    ];

    // Advanced configuration in YAML format.
    $form['options']['_advanced_configuration'] = [
      '#type' => 'textarea',
      '#title' => t('Advanced configuration'),
      '#default_value' =>  $options['_advanced_configuration'],
      '#rows' => 15,
      '#description' => t('Swiper configuration in YAML format. For possible options check <a href="https://swiperjs.com/swiper-api#parameters">documentation</a>. Any options set here will override previous options.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildPluginOptionsForm(array $form, FormStateInterface $form_state, BsSliderPluginOptionInterface $plugin_option) {
    if (empty($plugin_option->getPluginOptions('target_field_view_modes'))) {
      return [];
    }

    $elements = parent::buildPluginOptionsForm($form, $form_state, $plugin_option);

    $elements['view_mode'] = [
      '#type' => 'select',
      '#options' => $plugin_option->getPluginOptions('target_field_view_modes'),
      '#title' => t('View mode'),
      '#default_value' => $plugin_option->getPluginOptionValue('view_mode'),
      '#required' => TRUE,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function preprocess(array &$variables) {
    parent::preprocess($variables);

    $variables['items'] = $variables['element']['#items'];
    $variables['attributes']['id'] = $variables['element']['#id'];
    $variables['attributes']['data-bs-slider'] = 'swiper';

    $options = $this->getConfiguration();

    unset($options['_show_advanced_options']);

    // Merge advanced configuration.
    if (!empty($options['_advanced_configuration'])) {
      $options = array_merge($options, Yaml::parse($options['_advanced_configuration']));
    }
    unset($options['_advanced_configuration']);

    // Selected effect module.
    $effectModuleName = $options['effect'] . 'Effect';
//    if (isset($options[$effectModuleName])) {
//      $options[$effectModuleName]['enabled'] = TRUE;
//    }

    // Remove options with default values or disabled modules.
    $defaultOptions = $this->defaultConfiguration();
    foreach ($options as $key => $option) {
      if ($key === 'breakpoints') {
        if (empty($option)) {
          unset($options[$key]);
        }
      }
      elseif ($key === $effectModuleName) {
        if ($option == $defaultOptions[$key]) {
          unset($options[$key]);
        }
      }
      // Module options.
      elseif (is_array($option) && $key !== 'breakpoints') {
        // Remove all module options that are not enabled or have same defaults.
        if (empty($option['enabled']) || $option == $defaultOptions[$key]) {
          unset($options[$key]);
        }
      }
      // Remove all parameters that have default values.
      elseif (isset($defaultOptions[$key]) && $defaultOptions[$key] == $option) {
        unset($options[$key]);
      }
    }

    // Convert options that are numbers or boolean by type to its type.
    // We need to do this because for some values if we keep numbers like string
    // JS code will not work properly.
    $defaults = $this->defaultConfiguration();
    $this->convertToPrimitives($options, $defaults);

    // Thumbs support.
    if (!empty($variables['element']['#swiper_thumbs_id'])) {
      $options['_thumbs_id'] = $variables['element']['#swiper_thumbs_id'];
    }

    $variables['attributes']['data-bs-slider-options'] = json_encode($options);

    // Mark existing pagination, navigation and scrollbar for rendering.
    foreach (['pagination', 'navigation', 'scrollbar', '_lazyLoading'] as $template_option) {
      $variables[$template_option] = !empty($options[$template_option]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function view(array &$build, BsSliderConfigurationInterface $bs_slider, array $options = []) {
    parent::view($build, $bs_slider, $options);
    $build['#attached'] = ['library' => ['bs_slider_swiper/swiper']];
  }

  /**
   * Convert array of values to its primitives.
   *
   * Numbers are converted to int and float, 1 and 0 to boolean if default value
   * type is boolean and text values are kept.
   *
   * @param array $items
   *   Array of values to convert. If some value is array it will also be
   *   converted.
   *
   * @return void
   */
  protected function convertToPrimitives(array &$items, $defaults) {
    foreach($items as $key => &$item) {
      if (is_array($item)) {
        $this->convertToPrimitives($item, $defaults[$key] ?? []);
      }
      elseif (is_numeric($item)) {
        if (str_contains($item, '.')) {
          $item = (float) $item;
        }
        else {
          $item = (int) $item;
        }

        if (isset($defaults[$key]) && is_bool($defaults[$key])) {
          $item = (bool) $item;
        }
      }
    }
  }

}
