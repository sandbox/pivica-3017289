<?php

namespace Drupal\bs_slider_swiper\Plugin\BsSlider;

use Drupal\bs_slider\BsSliderPluginOptionInterface;
use Drupal\bs_slider\Entity\BsSliderConfigurationInterface;
use Drupal\bs_slider\Plugin\BsSliderBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

/**
 * Swiper thumbs plugin.
 *
 * @BsSlider(
 *   id = "swiper_thumbs_gallery",
 *   label = @Translation("Swiper Thumbs Gallery"),
 *   description = @Translation("Use Swiper as a thumbs gallery."),
 * )
 */
class BsSliderSwiperThumbsGallery extends BsSliderBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'gallery_id' => NULL,
      'thumbs_id' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $options = $this->getConfiguration();
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\bs_slider\BsSliderConfigurationManager $manager */
    $manager = \Drupal::service('bs_slider_configuration.manager');
    $configurations = $manager->getAllOptionSetByProperties(['plugin_id' => 'swiper']);

    $form['options']['gallery_id'] = [
      '#type' => 'select',
      '#title' => t('Gallery configuration'),
      '#required' => TRUE,
      '#options' => $configurations,
      '#default_value' => $options['gallery_id'],
      '#description' => t('BS Slider configuration of the swiper configuration used for gallery part.'),
    ];

    $form['options']['thumbs_id'] = [
      '#type' => 'select',
      '#title' => t('Thumbs Configuration'),
      '#required' => TRUE,
      '#options' => $configurations,
      '#default_value' => $options['thumbs_id'],
      '#description' => t('BS Slider configuration of the swiper configuration used for thumbs part.'),
    ];

    // TODO - thumbs module options.
    // @see https://swiperjs.com/swiper-api#thumbs-parameters

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildPluginOptionsForm(array $form, FormStateInterface $form_state, BsSliderPluginOptionInterface $plugin_option) {
    if (empty($plugin_option->getPluginOptions('target_field_view_modes'))) {
      return [];
    }

    $elements = parent::buildPluginOptionsForm($form, $form_state, $plugin_option);

    $elements['gallery_view_mode'] = [
      '#type' => 'select',
      '#options' => $plugin_option->getPluginOptions('target_field_view_modes'),
      '#title' => t('Gallery view mode'),
      '#default_value' => $plugin_option->getPluginOptionValue('gallery_view_mode'),
      '#required' => TRUE,
    ];

    $elements['thumbnail_view_mode'] = [
      '#type' => 'select',
      '#options' => $plugin_option->getPluginOptions('target_field_view_modes'),
      '#title' => t('Thumbnail view mode'),
      '#default_value' => $plugin_option->getPluginOptionValue('thumbnail_view_mode'),
      '#required' => TRUE,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function preprocess(array &$variables) {
    parent::preprocess($variables);

    $variables['thumbs_items'] = $variables['element']['#thumbs_items'];
    $variables['gallery_items'] = $variables['element']['#gallery_items'];
    $variables['gallery_items']['#swiper_thumbs_id'] = $variables['thumbs_items']['#id'];
  }

  /**
   * {@inheritdoc}
   */
  public function view(array &$build, BsSliderConfigurationInterface $bs_slider, array $options = []) {
    $gallery_items = [];
    $thumbnail_items = [];
    foreach (Element::children($build) as $delta) {
      $thumbnail_items[$delta] = $build[$delta];
      $gallery_items[$delta] = $build[$delta];

      if (!empty($build[$delta]['#view_mode'])) {
        $thumbnail_items[$delta]['#view_mode'] = $options['thumbnail_view_mode'];
        $gallery_items[$delta]['#view_mode'] = $options['gallery_view_mode'];

        // Changing view_mode cache key value we will invalidate cache tag
        // for this item and will trigger rendering if target view mode is
        // different.
        // @TODO - is there some better way to do this?
        if (isset($build[$delta]['#cache']['keys'][3])) {
          $thumbnail_items[$delta]['#cache']['keys'][3] = $options['thumbnail_view_mode'];
          $gallery_items[$delta]['#cache']['keys'][3] = $options['gallery_view_mode'];
        }
      }
    }

    $build = $this->buildSliderArray([], $bs_slider);
    $build['#attached'] = ['library' => ['bs_slider_swiper/swiper']];

    /** @var \Drupal\bs_slider\BsSliderConfigurationManagerInterface $manager */
    $manager = \Drupal::service('bs_slider_configuration.manager');

    // Thumbnail items.
    $build['#thumbs_items'] = $this->buildSliderArray($thumbnail_items, $manager->entityLoad($bs_slider->getOption('thumbs_id')));

    // Gallery items.
    $build['#gallery_items'] = $this->buildSliderArray($gallery_items, $manager->entityLoad($bs_slider->getOption('gallery_id')));
  }

}
